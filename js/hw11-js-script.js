function getListUl(){


let n =  +prompt( "Enter the number of points", "4" );
let arrList = [];


	for (let i = 0; i < n; i++){
		let points = prompt("Enter the text for your points", "");
		while(!points || points == 0) { // до тих пір доки не введено 0, ESC, пустий рядок
			points = prompt("Something wrong, enter the text for your points again", "");
		}
		arrList[i] = points;
	};

const pointsList = arrList.map(function(content){ // створили новий масив рointsList з елементами в списку 'ol'
	content = `<li class="li-style">${content}</li>`; // створили елемент 'li' нового масиву
    return content;				 // повернути створений елемент 'li' з введеним текстом
});
let olContent = pointsList.join(""); // переводимо масив назад в рядок

let ol = document.createElement('ol'); // створили список 'ol'
ol.classList.add("list-style"); 	   // додаємо стиль на створений список 'ol'
ol.innerHTML = olContent;
document.body.appendChild(ol);         // додали список 'ol' на сторінку


let timer = document.createElement("div"); // створюємо блок де буде розміщатись таймер
timer.style.color = "blue"; // стилізуємо таймер
let t = 9;                	// присвоюємо таймеру початкове значення для 10секунд
timer.classList.add("timer-style"); // стилізували і додали стилі для таймера

document.body.insertBefore(timer,ol); // вставили блок таймера перед створеним списком 'ol'

setInterval(function(){  // задаємо інтервал для таймера
    timer.innerText = t; // таймер буде показувати значення як відображення тексту
    if(t<5) {			 // якщо значення буде менше 5
        timer.style.color = "red"; // поміняти колір на червоний
    }
    t--;				// зменшення значення тексту (зменшення на одиницю)
},1000);				// через 1 секунду


setTimeout(() => {		// задаємо функцію видалення списку 'ol' через встановлений час
    ol.parentElement.removeChild(ol); // видалити список 'ol'
    timer.parentElement.removeChild(timer); // видалити список 'ol' з батьківського елемента через 10 сек
},10000);

}
getListUl();